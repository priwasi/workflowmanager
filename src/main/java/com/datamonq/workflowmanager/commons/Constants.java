package com.datamonq.workflowmanager.commons;

public abstract class Constants {
    public final static String MASTER_PERSISTENCE_UNIT_NAME="masterPersistenceUnitName";
    public final static String SLAVE_PERSISTENCE_UNIT_NAME="slavePersistenceUnitName";
    public final static String MASTER_TRANSACTION_MANAGER="masterTransactionManager";
    public final static String SLAVE_TRANSACTION_MANAGER="slaveTransactionManager";
    public final static String SUCCESS="SUCCESS";

}

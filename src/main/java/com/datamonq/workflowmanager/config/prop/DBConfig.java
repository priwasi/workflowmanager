package com.datamonq.workflowmanager.config.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("db")
public class DBConfig {
    private String masterUrl;
    private String masterPassword;
    private String masterUsername;
    private String masterDriverClass;
    private String masterPoolName;
    private int masterMaxPoolSize;
    private int masterMinIdle;
    private long masterConnectionTimeout;
    private String masterHibernateDilect;
    private String masterHibernateDdlAuto;
    private boolean matserHibernateShowSql;
    private long masterIdleConnTimeout;


    private String slaveUrl;
    private String slavePassword;
    private String slaveUsername;
    private String slaveDriverClass;
    private String slavePoolName;
    private int slaveMaxPoolSize;
    private int slaveMinIdle;
    private long slaveConnectionTimeout;
    private String slaveHibernateDilect;
    private String slaveHibernateDdlAuto;
    private boolean slaveHibernateShowSql;
    private long slaveIdleConnTimeout;


    public String getMasterUrl() {
        return masterUrl;
    }

    public void setMasterUrl(String masterUrl) {
        this.masterUrl = masterUrl;
    }

    public String getMasterPassword() {
        return masterPassword;
    }

    public void setMasterPassword(String masterPassword) {
        this.masterPassword = masterPassword;
    }

    public String getMasterUsername() {
        return masterUsername;
    }

    public void setMasterUsername(String masterUsername) {
        this.masterUsername = masterUsername;
    }

    public String getMasterDriverClass() {
        return masterDriverClass;
    }

    public void setMasterDriverClass(String masterDriverClass) {
        this.masterDriverClass = masterDriverClass;
    }

    public String getMasterPoolName() {
        return masterPoolName;
    }

    public void setMasterPoolName(String masterPoolName) {
        this.masterPoolName = masterPoolName;
    }

    public int getMasterMaxPoolSize() {
        return masterMaxPoolSize;
    }

    public void setMasterMaxPoolSize(int masterMaxPoolSize) {
        this.masterMaxPoolSize = masterMaxPoolSize;
    }

    public int getMasterMinIdle() {
        return masterMinIdle;
    }

    public void setMasterMinIdle(int masterMinIdle) {
        this.masterMinIdle = masterMinIdle;
    }

    public long getMasterConnectionTimeout() {
        return masterConnectionTimeout;
    }

    public void setMasterConnectionTimeout(long masterConnectionTimeout) {
        this.masterConnectionTimeout = masterConnectionTimeout;
    }

    public String getMasterHibernateDilect() {
        return masterHibernateDilect;
    }

    public void setMasterHibernateDilect(String masterHibernateDilect) {
        this.masterHibernateDilect = masterHibernateDilect;
    }

    public String getMasterHibernateDdlAuto() {
        return masterHibernateDdlAuto;
    }

    public void setMasterHibernateDdlAuto(String masterHibernateDdlAuto) {
        this.masterHibernateDdlAuto = masterHibernateDdlAuto;
    }

    public boolean isMatserHibernateShowSql() {
        return matserHibernateShowSql;
    }

    public void setMatserHibernateShowSql(boolean matserHibernateShowSql) {
        this.matserHibernateShowSql = matserHibernateShowSql;
    }

    public long getMasterIdleConnTimeout() {
        return masterIdleConnTimeout;
    }

    public void setMasterIdleConnTimeout(long masterIdleConnTimeout) {
        this.masterIdleConnTimeout = masterIdleConnTimeout;
    }

    public String getSlaveUrl() {
        return slaveUrl;
    }

    public void setSlaveUrl(String slaveUrl) {
        this.slaveUrl = slaveUrl;
    }

    public String getSlavePassword() {
        return slavePassword;
    }

    public void setSlavePassword(String slavePassword) {
        this.slavePassword = slavePassword;
    }

    public String getSlaveUsername() {
        return slaveUsername;
    }

    public void setSlaveUsername(String slaveUsername) {
        this.slaveUsername = slaveUsername;
    }

    public String getSlaveDriverClass() {
        return slaveDriverClass;
    }

    public void setSlaveDriverClass(String slaveDriverClass) {
        this.slaveDriverClass = slaveDriverClass;
    }

    public String getSlavePoolName() {
        return slavePoolName;
    }

    public void setSlavePoolName(String slavePoolName) {
        this.slavePoolName = slavePoolName;
    }

    public int getSlaveMaxPoolSize() {
        return slaveMaxPoolSize;
    }

    public void setSlaveMaxPoolSize(int slaveMaxPoolSize) {
        this.slaveMaxPoolSize = slaveMaxPoolSize;
    }

    public int getSlaveMinIdle() {
        return slaveMinIdle;
    }

    public void setSlaveMinIdle(int slaveMinIdle) {
        this.slaveMinIdle = slaveMinIdle;
    }

    public long getSlaveConnectionTimeout() {
        return slaveConnectionTimeout;
    }

    public void setSlaveConnectionTimeout(long slaveConnectionTimeout) {
        this.slaveConnectionTimeout = slaveConnectionTimeout;
    }

    public String getSlaveHibernateDilect() {
        return slaveHibernateDilect;
    }

    public void setSlaveHibernateDilect(String slaveHibernateDilect) {
        this.slaveHibernateDilect = slaveHibernateDilect;
    }

    public String getSlaveHibernateDdlAuto() {
        return slaveHibernateDdlAuto;
    }

    public void setSlaveHibernateDdlAuto(String slaveHibernateDdlAuto) {
        this.slaveHibernateDdlAuto = slaveHibernateDdlAuto;
    }

    public boolean isSlaveHibernateShowSql() {
        return slaveHibernateShowSql;
    }

    public void setSlaveHibernateShowSql(boolean slaveHibernateShowSql) {
        this.slaveHibernateShowSql = slaveHibernateShowSql;
    }

    public long getSlaveIdleConnTimeout() {
        return slaveIdleConnTimeout;
    }

    public void setSlaveIdleConnTimeout(long slaveIdleConnTimeout) {
        this.slaveIdleConnTimeout = slaveIdleConnTimeout;
    }
}
